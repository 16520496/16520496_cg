#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE


	SDL_RenderPresent(ren);



	
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;

	SDL_Rect rect[4];
	rect[0] = { 100,100, 10,10 };
	rect[1] = { 400,100, 10,10 };
	rect[2] = { 400,400, 10,10 };
	rect[3] = { 100,400, 10,10 };
	int choose = 0, motion = 0;
	SDL_Delay(2000);
	while (running)
	{
		SDL_Event e;
		if (SDL_PollEvent(&e))
		{
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			if (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT)
				for (int i = 0; i <= 3; i++)
					if (x >= rect[i].x&&y >= rect[i].y&&x <= rect[i].x + rect[i].w&&y < rect[i].y + rect[i].h)
					{
						motion = 1;
						choose = i;
					}
			if (e.type == SDL_MOUSEBUTTONUP && e.button.button == SDL_BUTTON_LEFT)motion = 0;
			if (motion)
			{
				rect[choose].x = x;
				rect[choose].y = y;
			}
			if (e.type == SDL_QUIT)running = false;
		}
		//draw background
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		//draw lines
		SDL_SetRenderDrawColor(ren, 238, 124, 107, 255);
		for (int i = 0; i < 3; i++)DDA_Line(rect[i].x, rect[i].y, rect[i + 1].x, rect[i + 1].y, ren);
		//draw curve
		SDL_SetRenderDrawColor(ren, 255, 0, 0, 255); //purple Curve
		Vector2D v(rect[0].x, rect[0].y);
		DrawCurve3(ren, Vector2D(rect[0].x, rect[0].y),
			Vector2D(rect[1].x, rect[1].y), Vector2D(rect[2].x, rect[2].y), Vector2D(rect[3].x, rect[3].y));
		//draw rects
		SDL_SetRenderDrawColor(ren, 244, 244, 0, 255); //yellow rect
		for (int i = 0; i < 4; i++)SDL_RenderFillRect(ren, &rect[i]);
		SDL_RenderPresent(ren);
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
