#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
//area 1
	int P = 1 - A;
	int x = 0;
	int y = 0;
	Draw2Points(xc,yc, x,y,ren);
	while (x < A)
	{
		if (P <= 0) P += 2 * x + 3;
		else {
			P += 2 * x + 3 - 2*A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	
//area 2
	
	while (x<400 && x>-400)
	{
		if (P <= 0) P += 2 * x + 3;
		else {
			P += 2 * y + 3 - 2 * A;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int P = 1 - A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (P <= 0) P += 2 * x + 3;
		else {
			P += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	while (x<400 && x>-400)
	{
		if (P <= 0) P +=  2*x + 3;
		else
		{
			P +=  2*y + 3 -2*A;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
