#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	double INC_PHI = 2 * M_PI / 3;
	double PHI = M_PI/2;

	int x[3];
	int y[3];
	for (int i = 0; i <3; i++)
	{
		x[i] = xc +int( R*cos(PHI)+0.5);
		y[i] = yc -int( R*sin(PHI)+0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < 3; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) %3], y[(i + 1) % 3], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	
	double INC_PHI = 2 * M_PI / 4;
	double PHI = M_PI / 4;

	int x[4];
	int y[4];
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	
	double INC_PHI = 2 * M_PI / 5;
	double PHI = M_PI / 2;

	int x[5];
	int y[5];
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{

	double INC_PHI = 2 * M_PI / 6;
	double PHI = M_PI / 2;

	int x[6];
	int y[6];
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < 6; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	
	double INC_PHI = 2 * M_PI / 5;
	double PHI = M_PI / 2;

	int x[5];
	int y[5];
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += INC_PHI;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	
	double INC_PHI = 2 * M_PI / 5;
	double PHI = M_PI / 2;
	double r = sin(M_PI / 10)*R / sin(7 * M_PI / 10);
	double PHInho = PHI + M_PI / 5;

	int x[5];
	int y[5];
	int xnho[5];
	int ynho[5];
	for (int i = 0; i <5;i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc+ int(r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], xnho[i],ynho[i] , ren);
		Bresenham_Line(xnho[i], ynho[i], x[(i + 1)%5], y[(i + 1)%5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	
	double INC_PHI = 2 * M_PI / 8;
	double PHI = M_PI / 2;
	double r = sin(M_PI / 8)*R / sin(6 * M_PI / 8);
	double PHInho = PHI + M_PI / 8;

	int x[8];
	int y[8];
	int xnho[8];
	int ynho[8];
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc +int( r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < 8; i++)
	{
		Bresenham_Line(x[i], y[i], xnho[i], ynho[i], ren);
		Bresenham_Line(xnho[i], ynho[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	double  INC_PHI = 2 * M_PI / 5;
	double PHI = startAngle;
	double r = sin(M_PI / 10)*R / sin(7 * M_PI / 10);
	double PHInho = PHI + M_PI / 5;

	int x[5];
	int y[5;
	int xnho[5];
	int ynho[5];
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);

		xnho[i] = xc + int(r*cos(PHInho)+0.5);
		ynho[i] = yc - int(r*sin(PHInho)+0.5);
		PHI += INC_PHI;
		PHInho += INC_PHI;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], xnho[i], ynho[i], ren);
		Bresenham_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	while (r > 1)
	{
		DrawStarAngle(xc, yc, r,phi, ren);
		r = sin(M_PI / 10)*r / sin(7 *M_PI / 10);
		phi += M_PI;
	}
}
